#!/usr/bin/env bash

# if apache2 does no exist
if [ ! -f /etc/apache2/apache2.conf ];
then

        cp /vagrant/conf/apt/sources.list /etc/apt/

        apt-get update

        # some sysutils
        apt-get install debconf-utils mailutils

        # some stuff for apt
        apt-get -y install python-software-properties

        # Install MySQL
        echo 'mysql-server-5.5 mysql-server/root_password password toor' | debconf-set-selections
        echo 'mysql-server-5.5 mysql-server/root_password_again password toor' | debconf-set-selections
        apt-get -y install mysql-client mysql-server-5.5

        # Install Apache
        apt-get -y install apache2

        # Install Git
        apt-get -y install git

        # Install PHP 
        apt-get -y install php5 libapache2-mod-php5 php-apc php5-mysql php5-dev curl

        # Install OpenSSL
        apt-get -y install openssl

        # Install PHP pear
        apt-get -y install php-pear

        # Install sendmail
        cat /vagrant/conf/postfix.preseed | debconf-set-selections
        apt-get -y install postfix

        # Install CURL dev package
        apt-get -y install libcurl4-openssl-dev

        # Install libssl, needed for zendbugger
        apt-get install libssl0.9.8

        # Install PECL HTTP (depends on php-pear, php5-dev, libcurl4-openssl-dev)x
        apt-get -y install make

        # Enable mod_rewrite    
        a2enmod rewrite

        # Enable SSL
        a2enmod ssl

        # Add www-data to vagrant group
        usermod -a -G vagrant www-data

        apt-get -y install imagemagick

        apt-get -y install ant
        apt-get -y install php5-xsl
        apt-get -y install php5-curl
        apt-get -y install php5-xdebug
        apt-get -y install php-http

        echo "xdebug.idekey = PHPSTORM" >> /etc/php5/conf.d/xdebug.ini
        echo "xdebug.remote_enable=1" >> /etc/php5/conf.d/xdebug.ini
        echo "xdebug.remote_connect_back = On" >> /etc/php5/conf.d/xdebug.ini
        echo "xdebug.max_nesting_level = 300" >> /etc/php5/conf.d/xdebug.ini

        # Add www-data to vagrant group
        usermod -a -G vagrant www-data
        usermod -a -G www-data vagrant

        # install symfony
        cd /var/www
        curl -s https://getcomposer.org/installer | php
        php composer.phar update -d symfony

        # install nginx + hhvm
        echo | add-apt-repository ppa:mapnik/boost
        wget -O - http://dl.hhvm.com/conf/hhvm.gpg.key | apt-key add -
        echo deb http://dl.hhvm.com/ubuntu precise main | tee /etc/apt/sources.list.d/hhvm.list
        apt-get update
        apt-get -y install nginx hhvm
        update-rc.d hhvm defaults

         # move our configfiles to etc and restart applications afterwards
        rsync -av /vagrant/conf/hhvm/* /etc/hhvm/
        rsync -av /vagrant/conf/nginx/* /etc/nginx/
        rm /etc/nginx/sites-enabled/default
        ln -s /etc/nginx/sites-available/hhvm /etc/nginx/sites-enabled/hhvm
        rsync -av /vagrant/conf/apache2/* /etc/apache2/
        rsync -av /vagrant/conf/php5/* /etc/php5/
        service hhvm restart
        service nginx restart
        service apache2 restart
        service mysql restart

         # And clean up apt-get packages
        apt-get -y clean
fi
